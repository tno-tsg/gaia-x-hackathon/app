import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import Ping from '../components/Ping.vue'
import Manufacturer from '../views/Manufacturer.vue'
import DPP from '../views/DPP.vue'

import Transporter from '../views/Transporter.vue'
import User from '../views/User.vue'



const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/home',
      name: 'Home',
      component: HomeView
    },
    {
      path: '/ping',
      name: 'Ping',
      component: Ping
    },
    {
      path: '/dpp',
      name: 'Digital Product Passport Dashboard',
      component: DPP
    },
  
    {
      path: '/manufacturer',
      name: 'Manufacturer',
      component: Manufacturer
    },
    {
      path: '/transporter',
      name: 'Transporter',
      component: Transporter
    },
    {
      path: '/user',
      name: 'User',
      component: User
    }
  ]
})

export default router


