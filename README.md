# Tech-X Product Passport demonstrator

## Local deployment

The application can be deployed locally via docker-compose.

### Requirements
* docker with docker-compose

### Deployment
In the repository root folder, run
```
docker-compose up
```

## Backend

Flask application

### Development setup

#### Requirements

Prerequisites:
* python (tested with 3.8) with venv

#### Installation
1. Create a virtualenv
```
python -m venv venv
```
(the application was tested with python 3.8)
2. Activate virtual environment
```
source venv/bin/activate
```
3. Install requirements
```
pip install -r requirements.txt -r requirements-dev.txt
```

#### Run application locally
1. Activate virtual environment
```
source venv/bin/activate
```
2. Run Flask app
```
FLASK_APP=app flask run
```
For auto-reload, also set the environment variable `FLASK_DEBUG` e.g. to `1`


## Test ID

http://localhost:5000/product/test
Query parameter: identifier -> urn:aas:techx:asset:Roller-0000