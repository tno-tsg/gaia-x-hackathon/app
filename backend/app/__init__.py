import os
from flask import Flask, jsonify
from flask_cors import CORS
from config import config

config_name = os.getenv("FLASK_CONFIG") or "default"




def create_app():
    app = Flask(__name__)
    app.config.from_object(config[config_name])

    CORS(app, resources={r'/*': {'origins': '*'}})

    from .healthcheck import healthcheck
    from .product import product

    app.register_blueprint(healthcheck, url_prefix="/healthcheck")
    app.register_blueprint(product, url_prefix="/product")

    return app


