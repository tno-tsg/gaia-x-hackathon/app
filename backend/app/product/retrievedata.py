import json
import random
import requests

connectorApiKey = "APIKEY-Uskl2jjdW8kl20ckA"
connectorAddress = "https://carbon-footprint-reader.techx.dataspac.es/ui/api"

identifier = "urn:aas:techx:aas:Roller-0008:A1-A3"

def retrieveConnectorInfo(identifier):
  sparqlQuery = "PREFIX ids: <https://w3id.org/idsa/core/> "\
                "SELECT (?r as ?resource) (CONCAT(str(?accessURL), ?path) as ?url) (?idsid as ?connectorId) " \
                "WHERE { " \
                "  GRAPH ?idsid { " \
                "    ?r a ids:DataResource. " \
                "    ?r ids:resourceEndpoint ?re. " \
                "    ?re ids:accessURL ?accessURL. " \
                "    ?re ids:path ?path. " \
                f"    FILTER(regex(str(?r), \"{identifier}\", \"i\" )) " \
                "  }" \
                "}"

  result = requests.post(url=f"{connectorAddress}/description/query/?recipientScope=ANY", data=sparqlQuery, headers={'Authorization': 'Bearer APIKEY-1AJ9AD3H37197H8C8CE0GA35'})

  jsonResult = result.json()

  jsonResult['results']['bindings'][0]

  return {
    'url': jsonResult['results']['bindings'][0]['url']['value'],
    'connectorId': jsonResult['results']['bindings'][0]['connectorId']['value']
  }

def retrieveMetadata(info, identifier):
  return requests.get(f'{connectorAddress}/description', {
    'connectorId': info['connectorId'],
    'accessUrl': info['url'],
    'requestedElement': identifier
  }, headers={'Authorization': 'Bearer APIKEY-1AJ9AD3H37197H8C8CE0GA35'}).json()

def requestContract(info, metadata):
  result = requests.post(url=f'{connectorAddress}/artifacts/consumer/contractRequest', files={
    'connectorId': (None, info['connectorId']),
    'contractOffer': (None, json.dumps(metadata['ids:contractOffer'][0])),
    'accessUrl': (None, info['url'])
  }, headers={'Authorization': 'Bearer APIKEY-1AJ9AD3H37197H8C8CE0GA35'})

  return result.json()

def requestArtifact(info, contractId, providerAgent, metadata):

  return requests.get(f'{connectorAddress}/artifacts/consumer/artifact', {
    'connectorId': info['connectorId'],
    'agentId': providerAgent,
    'accessUrl': info['url'],
    'artifact': metadata['ids:representation'][0]['ids:instance'][0]['@id'],
    'transferContract': contractId
  }, headers={'Authorization': 'Bearer APIKEY-1AJ9AD3H37197H8C8CE0GA35'})

usageCO2 = random.random()*5+5

def retrieveArtifact(identifier):
  global usageCO2
  info = retrieveConnectorInfo(identifier)

  metadata = retrieveMetadata(info, identifier)
  contract = requestContract(info, metadata)

  artifact = requestArtifact(info, contract['@id'], contract['ids:provider']['@id'], metadata)
  artifact = artifact.json()
  usageCO2 += random.random()+0.5
  if identifier.endswith('B1'):
    artifact['submodels'][1]['submodelElements'][0]['value'][1]['value'] = f"{usageCO2:.2f}"


  return artifact
