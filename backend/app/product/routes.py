import json
import os

from . import product
from . import retrievedata
from . import get_self_description

from flask import request


@product.route("/", methods=["GET"])
def get_product():
    with open(f"{os.path.dirname(__file__)}/../../mocks/product_data_for_frontend.json") as mock_file:
        return json.load(mock_file)

@product.route("/test", methods=["GET"])
def get_productAAS():
    identifier = request.args.get("identifier")
    # Get SD for product
    selfdescriptions = get_self_description.get_self_description(identifier)
    
    
    artifacts = []
    for idx, phase in enumerate(selfdescriptions['phases']):
        assets = phase['selfDescriptionCredential']['credentialSubject']['gx:aggregationOf']
        artifactId = [assetId for assetId in assets if assetId.startswith(identifier)][0]
        artifactId = artifactId.replace(':asset:', ':aas:')
        artifact = retrievedata.retrieveArtifact(artifactId)
        artifactCondensed = {value['idShort']: value['value'] for value in artifact['submodels'][1]['submodelElements'][0]['value']}
        artifactCondensed['Participant'] = selfdescriptions['participants'][idx]['selfDescriptionCredential']['verifiableCredential'][0]['credentialSubject']['gx:legalName']
        artifacts.append(artifactCondensed)
    
    artifacts = {value['PCFLifeCyclePhase']: value for value in artifacts}

    return {
        'full_version':{
            'selfdescriptions': selfdescriptions,
            'artifacts': artifacts
        },
        'short_version':{
            'selfdescriptions': {
                #Distributor
                selfdescriptions['participants'][0]['selfDescriptionCredential']['verifiableCredential'][0]['id']: {
                    "short":selfdescriptions['participants'][0]['selfDescriptionCredential']['verifiableCredential'][0]['credentialSubject'],
                    "full":selfdescriptions['participants'][0]
                },
                #manufacturer
                selfdescriptions['participants'][1]['selfDescriptionCredential']['verifiableCredential'][0]['id']: {
                    "short":selfdescriptions['participants'][1]['selfDescriptionCredential']['verifiableCredential'][0]['credentialSubject'],
                    "full":selfdescriptions['participants'][1]
                },
                #Usage
                selfdescriptions['participants'][2]['selfDescriptionCredential']['verifiableCredential'][0]['id']: {
                    "short":selfdescriptions['participants'][2]['selfDescriptionCredential']['verifiableCredential'][0]['credentialSubject'],
                    "full":selfdescriptions['participants'][2]
                }

                },
            'service_offering':{
                #Distributor
                selfdescriptions['participants'][0]['selfDescriptionCredential']['verifiableCredential'][0]['id']: {
                    "name": selfdescriptions['phases'][0]['selfDescriptionCredential']['credentialSubject']["gx:name"],
                    "providedBy":selfdescriptions['phases'][0]['selfDescriptionCredential']['credentialSubject']["gx:providedBy"]['id']
                },
                #Manufacturer
                selfdescriptions['participants'][1]['selfDescriptionCredential']['verifiableCredential'][0]['id']: {
                    "name": selfdescriptions['phases'][1]['selfDescriptionCredential']['credentialSubject']["gx:name"],
                    "providedBy":selfdescriptions['phases'][1]['selfDescriptionCredential']['credentialSubject']["gx:providedBy"]['id']
                },
                #Usage
                selfdescriptions['participants'][2]['selfDescriptionCredential']['verifiableCredential'][0]['id']: {
                    "name": selfdescriptions['phases'][2]['selfDescriptionCredential']['credentialSubject']["gx:name"],
                    "providedBy":selfdescriptions['phases'][2]['selfDescriptionCredential']['credentialSubject']["gx:providedBy"]['id']
                }
            },
            'artifacts':artifacts        
        }
    }
