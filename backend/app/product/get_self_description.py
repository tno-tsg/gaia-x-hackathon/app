import requests


def get_self_description(uid: str):
    assetSDresult = requests.get(f'https://gitlab.com/tno-tsg/gaia-x-hackathon/selfdescriptions/-/raw/master/{uid}')
    assetSD = assetSDresult.json()

    phaseSDs = []
    participantSDs = []
    for phase in assetSD['credentialSubject']['gx:aggregationOf']:
        try:
            phaseSDresult = requests.get(phase)
            phaseSD = phaseSDresult.json()
            phaseSDs.append(phaseSD)
            participantSD = requests.get(phaseSD['selfDescriptionCredential']['credentialSubject']['gx:providedBy']['id'])
            participantSDs.append(participantSD.json())
        except:
            print(f'Phase {phase} cant be resolved')

    return {
        'phases': phaseSDs,
        'participants': participantSDs
    }
