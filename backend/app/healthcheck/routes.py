from . import healthcheck


@healthcheck.route("/", methods=["GET"])
def healthcheck():

    return "The API is healthy"
