from flask import Blueprint

healthcheck = Blueprint("healthcheck", __name__)

from . import routes  # noqa E402
